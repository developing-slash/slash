#!/usr/bin/env bash
TESTS=../slash

if [ ! -f "$TESTS" ]; then
  echo Tests $TESTS
  exit 2
fi

killall node $TESTS > /dev/null

if [ -z "$(which curl)" ]; then
  echo Needs curl
  exit 2
fi

down () {
	if [ -n "$PID" ]; then
		kill $PID
		wait $PID
		rm .env
		echo 'Done'
	fi
}

suite () {
	SUITE="$1"

	down

	echo "$SUITE"
	[ -f .env ] && rm .env

	cat | ../slash > .env &

	export PID=$!
	STATUS=$?
	disown

	if [ $STATUS -gt 0 ]; then
		echo Not running. Exit status $STATUS
		exit 2
	fi

	if [ -z "$PID" ]; then
		echo No PID
		exit 2
	fi

	sleep 1

	if [ ! -f .env ]; then
		echo No .env
		exit 2
	fi

	ENV="$(cat .env | grep -v '^#')"
	export $ENV

	if [ -z "$PORT" ]; then
		echo No PORT
		exit 2
	fi

	echo PID $PID running on $PORT
}

test () {
  URL="$1"
  EXPECTED="$2"

	RESULT_=$(curl -D- -s http://localhost:$PORT$URL 2>&1 \
	| egrep -v '^(Date|Connection|Content-Length|Transfer-Encoding): ' \
	)
	RESULT=$(sed -e 's/[[:space:]]*$//' <<<"${RESULT_}")

  if [ "$RESULT" = "$EXPECTED" ]; then
		echo Pass "$URL"
	else
		echo Fail "$URL"
		echo EXPECTED _"$EXPECTED"_
		echo RESULT   _"$RESULT"_
		exit 1
	fi
}

suite /tests <<\🛑
/

# Given these programs, what responses should one expect?
🛑

test / 'HTTP/1.1 204 No content at endpoint'

suite '# greetings from slash' <<\🛑
/ Hello, World
🛑

test / 'HTTP/1.1 200 OK
Content-Type: text/plain

Hello, World'

suite '# multiline' <<\🛑
/ Hello, World,
hope you're having a lovely day!
🛑

test / "HTTP/1.1 200 OK
Content-Type: text/plain

Hello, World,
hope you're having a lovely day!"

suite '# interpolation and separation' <<\🛑
/ Hello, $user,
hope you're having a lovely day!

/user
🛑

test / "HTTP/1.1 200 OK
Content-Type: text/plain

Hello, \$user,
hope you're having a lovely day!"

test /user 'HTTP/1.1 204 No content at endpoint'

test /?user "HTTP/1.1 200 OK
Content-Type: text/plain

Hello, ,
hope you're having a lovely day!"

test /?user=Paul "HTTP/1.1 200 OK
Content-Type: text/plain

Hello, Paul,
hope you're having a lovely day!"

test /?u=Paul "HTTP/1.1 200 OK
Content-Type: text/plain

Hello, \$user,
hope you're having a lovely day!"

suite '# JSON and matching' <<\🛑
# Consider $USER from environment

/ { "greeting": "Hello", name: "World" }

/?user { "greeting": "Welcome", name: "$user" }
🛑

test / 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Hello", name: "World" }'

test /?user 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Welcome", name: "" }'

test /?user=Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Welcome", name: "Paul" }'

test /?user=\"Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Welcome", name: ""Paul" }'

test /?User=\"Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Hello", name: "World" }'

suite '# specificity' <<\🛑
/?user { "greeting": "Welcome", name: "$user" }

/?user=Paul { "greeting": "Hi", name: "P" }

/ { "greeting": "Hello", name: "World" }

/?user=P { "greeting": "Booyah", name: "P" }
🛑

test / 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Hello", name: "World" }'

test /?user 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Welcome", name: "" }'

test /?user=Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "greeting": "Hi", name: "P" }'

suite '# HTML' <<\🛑
/ <h1>Hello, World!</h1>
<nav><ul>
 <li><a href=/users>Users</a>
</ul></nav>

/?user <h1>Welcome, $user</h1>
<nav><ul>
 <li><a href=/users/$user>Your homepage</a>
</ul></nav>

/users <h1>Find User</h1>
<form>Find <input name=user></form>

/users?user <h1>Sorry</h1>
<p>$user is not a user

/users?user=P <h1>Success</h1>
<nav><ul>
 <li><a href=/users/P>The P Page</a>
</ul></nav>

/users/P <h1>The P Page</h1>
🛑

test / 'HTTP/1.1 200 OK
Content-Type: text/html

<h1>Hello, World!</h1>
<nav><ul>
 <li><a href=/users>Users</a>
</ul></nav>'

test /?user=P 'HTTP/1.1 200 OK
Content-Type: text/html

<h1>Welcome, P</h1>
<nav><ul>
 <li><a href=/users/P>Your homepage</a>
</ul></nav>'

test /users?user=Paul 'HTTP/1.1 200 OK
Content-Type: text/html

<h1>Sorry</h1>
<p>Paul is not a user'

test /users/P 'HTTP/1.1 200 OK
Content-Type: text/html

<h1>The P Page</h1>'

suite '# JSON array' <<\🛑
/users [
 { "name": "P", "link": "/users/P", "title": "The P Page" }
]
🛑

test /users 'HTTP/1.1 200 OK
Content-Type: application/json

[
 { "name": "P", "link": "/users/P", "title": "The P Page" }
]'

suite '# lookup, matcher result' <<\🛑
/user/P { "name": "P", "title": "The P Page" }

/user/Chon { "name": "Chon", "title": "Chon Welcomes" }

/user/Yxta { "name": "Yxta", "title": "This is Yxta" }

/user/?name { "name": "$name", "title": "Hello from $name" }

/user/?name&title { "name": "$name", "title": "$title" }

/users [
  @/user/P
, @/user/Chon
, @/user/Yxta
, @/user/?name
]
🛑

test /users 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "", "title": "Hello from " }
]'

test /user/?name=Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "name": "Paul", "title": "Hello from Paul" }'

test /users 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "", "title": "Hello from " }
]'

curl -sd '' http://localhost:$PORT/
echo 'Pass POST /'

test /user/?name=Paul 'HTTP/1.1 200 OK
Content-Type: application/json

{ "name": "Paul", "title": "Hello from Paul" }'

test /users 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "Paul", "title": "Hello from Paul" }
]'

test /users?name 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "Paul", "title": "Hello from Paul" }
]'

test /user/?name=P\&title=Booyah+from+P 'HTTP/1.1 200 OK
Content-Type: application/json

{ "name": "P", "title": "Booyah from P" }'

test /users 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "Paul", "title": "Hello from Paul" }
]'

test /user/?name=P 'HTTP/1.1 200 OK
Content-Type: application/json

{ "name": "P", "title": "Hello from P" }'

test /users?name 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "Paul", "title": "Hello from Paul" }
]'

test /users?title 'HTTP/1.1 200 OK
Content-Type: application/json

[
  { "name": "P", "title": "The P Page" }
, { "name": "Chon", "title": "Chon Welcomes" }
, { "name": "Yxta", "title": "This is Yxta" }
, { "name": "P", "title": "Hello from P" }
]'

suite '# lookup, self' <<\🛑
/concat?a&b $a$b

/ @/concat?a=Hello,+&b=world

/last @/concat?a&b

/nothing @/concat
🛑

test /concat 'HTTP/1.1 404 Endpoint not found'

test /concat?a=Hello 'HTTP/1.1 404 Endpoint not found'

test /concat?b=Hello 'HTTP/1.1 404 Endpoint not found'

test /concat?a\&b 'HTTP/1.1 200 OK
Content-Type: text/plain'

test /concat?b=+Doo\&a=Whoop+De 'HTTP/1.1 200 OK
Content-Type: text/plain

Whoop De Doo'

test /last 'HTTP/1.1 200 OK
Content-Type: text/plain

Whoop De Doo'

test / 'HTTP/1.1 200 OK
Content-Type: text/plain

Hello, world'

test /last 'HTTP/1.1 200 OK
Content-Type: text/plain

Whoop De Doo'

test /last?why 'HTTP/1.1 200 OK
Content-Type: text/plain

Hello, world'

suite '# lookup, content type' <<\🛑
/index.html <html>This is index.html. <a href="/about">About</a></html>

/about.html <html><head><title>About</title></head><body>There is only <a href="/">index.html</a>!</body></html>

/ @/index.html

/about @/about.html

/self @/index.html?self
🛑

test / 'HTTP/1.1 200 OK
Content-Type: text/html

<html>This is index.html. <a href="/about">About</a></html>'

test /about 'HTTP/1.1 200 OK
Content-Type: text/html

<html><head><title>About</title></head><body>There is only <a href="/">index.html</a>!</body></html>'

test /self 'HTTP/1.1 200 OK
Content-Type: text/html

<html>This is index.html. <a href="/about">About</a></html>'

down
